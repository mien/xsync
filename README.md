#xsync
使用epoll+动态线程池实现的用以同步项目文件的应用。

### 可应用的场景
* 文件上传
* 异步任务执行

###待做:
* 各线程工作状态标记，更合理使用信号量
* 各语言的客户端代码
* below:
* header, Tag 标签
* bytesWritten, bytesTotal
* PROGRESS
* enctype/multipart

###联系
EMAIL: m@luym.cn

