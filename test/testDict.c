/**
 * 测试dict
 * gcc -o testDict testDict.c ../dict.c
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include "../dict.h"

int main(int argc, char* argv[])
{

    char buf[1024];
    char key[20];
    char val[256];

    dictTable *d = dictCreate(&dictTypeStringKeyVal);

    FILE *f = fopen("../conf/mime.types", "r");
    int j = 0;
    while (fgets(buf, 1024, f)) {
        char *p = NULL;
        int k = 0;
        while (buf[k] == ' ') k++;
        if (buf[k] == '\n') continue;
        p = buf + k;
        while(buf[k] != ' ') k++;
        strncpy(val, p, buf + k - p);
        val[buf + k - p] = '\0';

        while (1) { 
            while (buf[k] == ' ') k++;
            p = buf + k;
            while (buf[k] != ' ' && buf[k] != '\n') k++;
            strncpy(key, p, buf + k - p);
            key[buf + k - p] = '\0';
            dictSet(d, key, val);
            j++;
            if (buf[k] == '\n') break;
        }
    }
    fclose(f);
    
    //dictDel(d, "pm");
    //dictDel(d, "msp");
    //dictDel(d, "xml");

    printf("-------------------------------\n");
    printf(" dict info:\n");
    printf("-------------------------------\n");

    int max_chain_len = 0;
    int i;
    for (i = 0; i < d->size; i++) {
        printf("bucket %d: ", i);
        int chain_len  = 0;
        dictNode *n = d->ht[i];
        while (n) {
            printf("%s->%s, ", n->key, n->val);
            chain_len++;
            n = n->next;
        }
        if (chain_len > max_chain_len)
            max_chain_len = chain_len;
        printf("\n");
    }

    printf("size: %d\n", d->size);
    printf("used: %d\n", d->used);
    printf("max chain len: %d\n", max_chain_len);
    
    if (argc > 1) {
        printf("%s: %s\n", argv[1], dictGet(d, argv[1]));
    }

    dictFree(d);
    return 0;
}
