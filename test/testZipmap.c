/**
 * 测试zipmap
 * gcc -o zipmap testZipmap.c ../src/zipmap.c
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include "../src/zipmap.h"

int main(int argc, char* argv[])
{
    char buf[1024];
    zipInit(buf, sizeof(buf));
    printf("zip length:%d\n\n", zipNum(buf));

    zipAdd(buf, "name", "luyamin");
    zipAdd(buf, "age", "36");
    zipAdd(buf, "domain", "www.fcniu.com");
    printf("domain: %s\n", zipGet(buf, "domain"));
    printf("name: %s\n", zipGet(buf, "name"));
    printf("age: %s\n", zipGet(buf, "age"));
    printf("zip length:%d\n\n", zipNum(buf));
    printf("\n");

    zipInit(buf, sizeof(buf));

    zipAdd(buf, "name", "lupeiyu");
    zipAdd(buf, "age", "2");
    zipAdd(buf, "telephone", "18600410018");
    zipAdd(buf, "gender", "female");
    zipAdd(buf, "long", "female1234female1234female1234female1234female1234female1234female1234female1234female1234female1234female1234female1234female1234female1234female1234female1234female1234female1234female1234female1234THISISTAIL!");
    printf("name: %s\n", zipGet(buf, "name"));
    printf("age: %s\n", zipGet(buf, "age"));
    printf("telephone: %s\n", zipGet(buf, "telephone"));
    printf("gender: %s\n", zipGet(buf, "gender"));
    printf("long: %s\n", zipGet(buf, "long"));
    printf("zip length:%d\n\n", zipNum(buf));

    printf("-----iter--------\n");
    zipReset(buf);
    char *key, *val;
    while (zipNext(buf, &key, &val)) {
        printf("key: %s, val: %s\n", key, val);
    }
}
