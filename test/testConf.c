/**
 * 测试dict
 * gcc -o testConf testConf.c ../config.c ../dict.c
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include "../src/dict.h"
#include "../src/config.h"

extern dictTable *_project, *_mimes;
extern char _server[512];

int main(int argc, char* argv[])
{
    loadConfig("../conf/xsync.conf");
    dictTable *d = _mimes;

    printf("-------------------------------\n");
    printf(" zip info:\n");
    printf("-------------------------------\n");
    printf("host: %s\n", getServerConfig("host"));
    printf("default: %s\n", getServerConfig("default_project"));

    printf("-------------------------------\n");
    printf(" dict info:\n");
    printf("-------------------------------\n");

    int max_chain_len = 0;
    int i;
    for (i = 0; i < d->size; i++) {
        printf("bucket %d: ", i);
        int chain_len  = 0;
        dictNode *n = d->ht[i];
        while (n) {
            printf((n->next ? "%s->%s, " : "%s->%s"), n->key, n->val);
            chain_len++;
            n = n->next;
        }
        if (chain_len > max_chain_len)
            max_chain_len = chain_len;
        printf("\n");
    }

    printf("size: %d\n", d->size);
    printf("used: %d\n", d->used);
    printf("max chain len: %d\n", max_chain_len);
    
    if (argc > 1) {
        xsyncProject *pro = getProjectConfig(argv[1]);
        if (pro == NULL)
            printf("project %s not found\n", argv[1]);
        else
            printf("%s->auth:%s; path:%s\n", argv[1], pro->auth, pro->path);
    }

    freeConfig();
    return 0;
}
