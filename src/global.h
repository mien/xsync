/*
 * =====================================================================================
 *       Filename:  config.h
 *    Description:  全局定义
 *        Created:  2017-11-06 21:45
 *         Author:  mien, m@luym.cn
 * =====================================================================================
 */

#ifndef GLOBAL_H
#define GLOBAL_H

#define XSYNC_VERSION "1.2.0"

#define DEBUGGING 1

#if DEBUGGING
#define DEBUG(message) \
    printf message;
#else
#define DEBUG(message) \
    ;
#endif

#endif /* GLOBAL_H */
