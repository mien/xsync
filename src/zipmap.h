/*
 * =====================================================================================
 *       Filename:  zipmap.h
 *         Author:  MIEN
 *    Description:  在一个缓冲区中，存储少量的字符串到字符串的映射(string->string)，可添加、查询和遍历。
 *                  可直接声明定长的缓冲区使用，不必频繁地申请和释放内存。
 *           Date:  2015-07-07
 * =====================================================================================
 */
#ifndef ZIPMAP_H
#define ZIPMAP_H

int zipAdd(char *buf, char *key, char *val);
int zipNum(char *buf);
int zipInit(char *buf, int size);
char *zipGet(char *buf, char *key);

void zipReset(char *buf);
int zipNext(char *buf, char **key, char **val);

#endif /* ZIPMAP_H */
