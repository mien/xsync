/*
 * =====================================================================================
 *       Filename:  net.h
 *    Description:  net header file
 * =====================================================================================
 */
#ifndef NET_H
#define NET_H

typedef void tcpHandler(int fd);
int tcpListen(const char* host, const char* port, tcpHandler* handler);

#endif
