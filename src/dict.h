/*
 * =====================================================================================
 *       Filename:  dict.h
 *         Author:  MIEN
 *           Date:  2015-07-02
 * =====================================================================================
 */
#ifndef DICT_H
#define DICT_H

typedef struct dictNode {
    void *key;
    void *val;
    struct dictNode *next;
} dictNode;

typedef struct dictType {
    unsigned int (*keyHash) (void *key);
    int (*keyCompare) (void *key1, void *key2);
    void *(*keyDup) (void *key);
    void (*keyDel) (void *key);
    void *(*valDup) (void *val);
    void (*valDel) (void *val);
} dictType;

typedef struct dictTable {
    dictType *type;
    dictNode **ht;
    int size;
    int sizemask;
    int used;
} dictTable;

dictTable *dictCreateWithSize(dictType *type, int size);
dictTable *dictCreate(dictType *type);
void dictFree(dictTable *dt);

void *dictGet(dictTable *dt, void *key);
void dictSet(dictTable *dt, void *key, void *val);
void dictDel(dictTable *dt, void *key);

extern dictType dictTypeStringKey;
extern dictType dictTypeStringKeyVal;
extern dictType dictTypeStringKeyFreeVal;

#define DICT_DEFAULT_SIZE 8
#define DICT_MAXIMUM_SIZE (1<<16)

#endif /* DICT_H */
