/*
 * =====================================================================================
 *       Filename:  executer.h
 *    Description:  命令执行代码头文件
 *        Created:  2017-17-05 13:51
 *         Author:  mien, m@luym.cn
 * =====================================================================================
 */

#ifndef EXECUTER_H
#define EXECUTER_H

void initExecuter();
void waitExecuter();

#define EX_BUF_SIZE 2048

#endif
