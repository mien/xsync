/*
 * =====================================================================================
 *       Filename:  pool.h
 *    Description:  服务处理主代码
 *        Created:  2015-07-07 10:00
 *         Author:  mien, m@luym.cn
 * =====================================================================================
 */

#include <pthread.h>
#include <semaphore.h>
#include "config.h"

#ifndef HANDLE_H
#define HANDLE_H

/* 线程池中的最小线程数 */
#define SPARE_THREADS_NUM 1

/* 某一客户端请求的处理任务 */
typedef struct clientRequestTask {
    int fd;

    char line[4096];
    char path[2048];
    char query[1024];
    char method[16];
    char uri[1024];
    char key[128];
    char value[256];
    char headers[4096];

    unsigned int lw; /* lenght of last written bytes to client */
    unsigned int lr; /* length of already read bytes from client */
    unsigned int contentLength;

    xsyncProject *xp;

    int exit;
    sem_t sem;
    pthread_t thread_id;
    
    struct clientRequestTask *next;
} clientRequestTask; 

/* 线程池操作 */
void poolInit();
void poolHandle(int fd);
int poolDestroy();

#endif
