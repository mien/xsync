void httpHandle(clientRequestTask *t)
{
    int n;
    n = getLine(t);

    char* query_string = NULL;
    parseMUQ(t->line, t->method, sizeof(t->method), t->uri, sizeof(t->uri), t->query, sizeof(t->query));

    if (!strstr(getServerConfig("verbs"), t->method)) {
        httpResponse(t, 406, NULL, NULL, 0);
        discardLeft(t);
        close(t->fd);
        return;
    }

    DEBUG(("fd:%d, errno:%d, thread_id:%d, method: %s, uri: %s, query: %s\n", t->fd, errno, t->thread_id, t->method, t->uri, t->query))

    zipInit(t->headers, sizeof(t->headers));
    t->contentLength = 0;
    t->lr = 0;

    while (1) {
        n = getLine(t);
        if (n <= 0 || strcmp("\n", t->line) == 0)
            break;
        parseKV(t->line, t->key, 128, t->value, 256);
        zipAdd(t->headers, t->key, t->value);
        DEBUG(("%s: %s\n", t->key, t->value))
    }

    char *cl = zipGet(t->headers, "Content-Length");
    char *project = zipGet(t->headers, "Project");
    char *auth = zipGet(t->headers, "Auth");

    if (cl) {
        t->contentLength = atoi(cl);
    }

    if (project == NULL) {
        project = getServerConfig("default_project");
    }

    if (project == NULL || (t->xp = getProjectConfig(project)) == NULL) {
        httpResponse(t, 406, NULL, NULL, 0);
    }
    else if (strcmp(t->xp->auth, "none") && /* 需要验证 */
             (auth == NULL || strcmp(auth, t->xp->auth)) /* 密码不匹配 */
            ) {
        httpResponse(t, 401, NULL, NULL, 0);
    }
    else {
        httpCommand *cmd = dictGet(_commands, t->method);
        if (cmd != NULL) {
            cmd->proc(t);
        }
        else {
            httpResponse(t, 501, NULL, NULL, 0);
        }
    }

    discardLeft(t);
    close(t->fd);
}

