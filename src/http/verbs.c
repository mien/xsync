static pthread_mutex_t _execLock;

#include "_get.c"
#include "_put.c"
#include "_post.c"
#include "_head.c"
#include "_delete.c"
#include "_exec.c"
#include "_options.c"

static dictTable *_commands;
static httpCommand _httpCommandTable[] = {
    {"GET", httpGet},
    {"PUT", httpPut},
    {"POST", httpPost},
    {"HEAD", httpHead},
    {"DELETE", httpDelete},
    {"EXEC", httpExec},
    {"OPTIONS", httpOptions}
};
