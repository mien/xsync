void httpPut(clientRequestTask *t)
{
    sprintf(t->path, "%s%s", t->xp->path, t->uri);

    mode_t mode;
    struct stat st;
    int i, len = strlen(t->path);
    int flag = 0;

    // 逐级检查目录，不存在则建立
    for (i = 0; i < len; i++) {
        if (t->path[i] == '/') {
            t->path[i] = '\0';
            if (access(t->path, F_OK) == 0) {
                stat(t->path, &st);
                if ((st.st_mode & S_IFMT) != S_IFDIR) {
                    sprintf(t->line, "3001, Path '%s' already exists, and is not a directory!", t->path);
                    flag = 1;
                    break;
                }
                mode = st.st_mode & 0777;
            }
            else {
                mkdir(t->path, mode);
            }
            t->path[i] = '/';
        }
    }
    
    // 检查目录无错误，写数据到文件
    if (!flag) {
        int isFile = 0;
        if (t->path[len - 1] != '/') {
            if (stat(t->path, &st) == -1) {
                isFile = 1;
            }
            else if ((st.st_mode & S_IFMT) == S_IFREG) {
                isFile = 1;
            }
        }
        if (isFile) {
            int n = 0;
            if (t->contentLength > 0){
                n = saveFile(t);
            }
            if (n < 0)
                sprintf(t->line, "3003, Put file '%s' failed!", t->path);
            else 
                sprintf(t->line, "3000, Put file '%s' successfully, %d bytes written!", t->path, n);
        }
        else {
            sprintf(t->line, "3002, '%s' is a directory!", t->path);
        }
    }

    httpResponse(t, 200, t->line, NULL, strlen(t->line));
}
