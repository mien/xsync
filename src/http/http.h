/*
 * =====================================================================================
 *       Filename:  http.h
 * =====================================================================================
 */
#include "../pool.h"

#ifndef HTTP_H
#define HTTP_H

static struct {
    int code;
    char *text;
} http_status_list[] = {
    {200, "OK"},
    {400, "Bad Request"},
    {401, "Unauthorized"},
    {404, "Not Found"},
    {405, "Method Not Allowed"},
    {406, "Not Acceptable"},
    {500, "Internal Server Error"},
    {501, "Not Implemented"}
};

typedef struct {
    char *name;
    void (*proc) (clientRequestTask *t);
} httpCommand;

void httpHandle(clientRequestTask *t);
void cmdInit();
void cmdFree();

#endif
