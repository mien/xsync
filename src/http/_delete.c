void httpDelete(clientRequestTask *t)
{
    sprintf(t->path, "%s%s", t->xp->path, t->uri);

    struct stat st;
    int len = strlen(t->path);
    if (t->path[len - 1] != '/') {
        if (stat(t->path, &st) == -1) {
            sprintf(t->line, "4002, File '%s' does not exist!", t->path);
        }
        else if ((st.st_mode & S_IFMT) == S_IFREG) {
            if (-1 == unlink(t->path))
                sprintf(t->line, "4001, Delete file '%s' failed!", t->path);
            else
                sprintf(t->line, "4000, Delete file '%s' successfully!", t->path);
        }
        else if ((st.st_mode & S_IFMT) == S_IFDIR) {
            sprintf(t->line, "4003, Can not delete directory '%s'!", t->path);
        }
        else {
            sprintf(t->line, "4004, '%s' is not a regular file!", t->path);
        }
    }
    else {
        sprintf(t->line, "4005, Can not delete directory '%s'!", t->path);
    }

    httpResponse(t, 200, t->line, NULL, strlen(t->line));
}
