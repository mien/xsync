void httpHead(clientRequestTask *t)
{
    sprintf(t->path, "%s%s", t->xp->path, t->uri);
    if (t->path[strlen(t->path) - 1] == '/') {
        strcat(t->path, "index.html");
    }

    struct stat st;
    if (stat(t->path, &st) == -1) {
        httpResponse(t, 404, NULL, NULL, 0);
    } else {
        if ((st.st_mode & S_IFMT) == S_IFREG) {
            char *ext = strrchr(t->path, '.'); /* extension */
            char *ct = getMimesConfig(ext ? ext + 1: "html");
            httpResponse(t, 200, NULL, ct, st.st_size);
        }
        else {
            httpResponse(t, 404, NULL, NULL, 0);
        }
    }
}
