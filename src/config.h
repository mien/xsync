/*
 * =====================================================================================
 *       Filename:  config.h
 *    Description:  配置相关代码
 *        Created:  2015-01-10 10:50
 *         Author:  mien, m@luym.cn
 * =====================================================================================
 */

#ifndef CONFIG_H
#define CONFIG_H

/* 加载配置文件 */
void loadConfig(const char *program, const char *conf);
void freeConfig();

/* 设置进程的用户身份 */
void setUidGid();

/* 项目 */
typedef struct {
    char name[32];
    char auth[32];
    char path[256];
} xsyncProject;

/* 获取设置的方法 */
char *getServerConfig(char *key);
xsyncProject *getProjectConfig(char *name);
char *getMimesConfig(char *ext);
char *getHeadersConfig();

#endif /* CONFIG_H */
