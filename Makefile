CC = gcc

vpath %.c src
vpath %.c src/http

xsync: xsync.c executer.o pool.o http.o net.o config.o dict.o zipmap.o
	$(CC) -o $@ $^ -lpthread

%.o : %.c
	$(CC) -c $<

clean:
	rm -fr xsync *.o
	
